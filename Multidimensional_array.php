<?php

echo '<b>Example-1:</b><br >';

$marks = array( 
            "mohammad" => array
            (
               "physics" => 35,
               "maths" => 30,	
               "chemistry" => 39
            ),
            
            "qadir" => array
            (
               "physics" => 30,
               "maths" => 32,
               "chemistry" => 29
            ),
            
            "zara" => array
            (
               "physics" => 31,
               "maths" => 22,
               "chemistry" => 39
            )
         );
         
         /* Accessing multi-dimensional array values */
         echo "Marks for mohammad in physics : " ;
         echo $marks['mohammad']['physics'] . "<br />"; 
         
         echo "Marks for qadir in maths : ";
         echo $marks['qadir']['maths'] . "<br />"; 
         
         echo "Marks for zara in chemistry : " ;
         echo $marks['zara']['chemistry'] . "<br />"; 
         echo "<br>";


echo '<b>Example-2:</b><br >';

$cars = array
   (
   array("Volvo",22,18),
   array("BMW",15,13),
   array("Saab",5,2),
   array("Land Rover",17,15)
   );
  
echo $cars[0][0].": In stock: ".$cars[0][1].", sold: ".$cars[0][2].".<br>";
echo $cars[1][0].": In stock: ".$cars[1][1].", sold: ".$cars[1][2].".<br>";
echo $cars[2][0].": In stock: ".$cars[2][1].", sold: ".$cars[2][2].".<br>";
echo $cars[3][0].": In stock: ".$cars[3][1].", sold: ".$cars[3][2].".<br>";
echo "<br>";


echo '<b>Example-3:</b><br >';

$cars = array
   (
   array("Volvo",22,18),
   array("BMW",15,13),
   array("Saab",5,2),
   array("Land Rover",17,15)
   );
    
for ($row = 0; $row <  4; $row++) {
   echo "<p><i>Row number $row</i></p>";
   echo "<ul>";
   for ($col = 0; $col <  3; $col++) {
     echo "<li>".$cars[$row][$col]."</li>";
   }
   echo "</ul>";
}
echo "<br>";


echo '<b>Example-4:</b><br >';

   //Initialize the array using the array() function.
$flower_shop = array(
"rose" => array( "5.00", "7 items", "red" ),
"daisy" => array( "4.00", "3 items", "blue" ),
"orchid" => array( "2.00", "1 item", "white" ),
);

//print "rose costs 5.00, and you get 7 items."
echo "rose costs ".$flower_shop['rose'][0].", and you get ".$flower_shop['rose'][1].".";
echo "<br>";
//print "daisy costs 4.00, and you get 3 items."
echo "daisy costs ".$flower_shop['daisy'][0].", and you get ".$flower_shop['daisy'][1].".";
echo "<br>";
//print "orchild costs 2.00, and you get 1 item.
echo "orchid costs ".$flower_shop['orchid'][0].", and you get ".$flower_shop['orchid'][1].".";      

echo "<br>";


echo '<b>Example-5:</b><br >';

$categories = array( array ( array( 'CAR_TIR', 'Tires', 100 ),
               array( 'CAR_OIL', 'Oil', 10 ),
               array( 'CAR_SPK', 'Spark Plugs', 4 )
              ),
           array ( array( 'VAN_TIR', 'Tires', 120 ),
               array( 'VAN_OIL', 'Oil', 12 ),
               array( 'VAN_SPK', 'Spark Plugs', 5 )
              ),
           array ( array( 'TRK_TIR', 'Tires', 150 ),
               array( 'TRK_OIL', 'Oil', 15 ),
               array( 'TRK_SPK', 'Spark Plugs', 6 )
              )
          );
for ( $layer = 0; $layer < 3; $layer++ )
{
 echo "Layer $layer<br />";
 for ( $row = 0; $row < 3; $row++ )
 {
  for ( $column = 0; $column < 3; $column++ )
  {
   echo '|'.$categories[$layer][$row][$column];
  }
  echo '|<br />';
 }
}

?>